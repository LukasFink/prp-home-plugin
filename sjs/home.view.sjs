/*
        PRP Project
        Copyright (C) 2020  The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

class HomeView {

    constructor(translationLoader) {
        this.trans = translationLoader;
        this.service = new HomeService();
        this.model = new HomeModel(this.service);
        this.controller = new HomeController(this.service)

        this.createNotificationList();
    }

    createHomeTitle() {
        let homePageTitleTemplate = document.getElementById('homePageTitleTemplate').content;
        let homePageTitleTemplateCopy = document.importNode(homePageTitleTemplate, true);
        homePageTitleTemplateCopy.querySelector(".page-title").textContent = this.trans.home.title;
        document.getElementById("titleArea").appendChild(homePageTitleTemplateCopy);
    }

    createNotificationList() {
        let notificationsNotSorted = PLUGINMANAGER.getNotifications();
        let notifications = notificationsNotSorted.sort(function sortingByPriority(not1, not2) {
            return not1.priority - not2.priority;
        });

        for (let i = 0; i < notifications.length; i++) {
            let notification = notifications[i];

            let notificationCardTemplate = document.getElementById('notificationTemplate').content;
            let templateCopy = document.importNode(notificationCardTemplate, true);

            templateCopy.querySelector('.notificationCard').setAttribute('onclick', 'gotoLink("' + notification.link + '")');
            templateCopy.querySelector('.notificationColorBar').setAttribute('style', 'background: ' + notification.color);
            templateCopy.querySelector('.notificationIcon').setAttribute('class', 'notificationIcon ' + notification.icon);
            templateCopy.querySelector('.notificationTitle').textContent = notification.title;
            templateCopy.querySelector('.notificationText').textContent = notification.text;

            document.getElementById('notificationList').appendChild(templateCopy);
        }
    }

}