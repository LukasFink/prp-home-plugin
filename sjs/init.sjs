/*
        PRP Project
        Copyright (C) 2020  The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

PLUGINMANAGER.register("homePlugin", function () {

    this.name = "Home";
    this.iconClass = "fas fa-home";
    this.homeUrl = "/";
    this.ranking = 100;
    this.filePath = "home.html";

});

function toggleNav() {
    document.getElementById('sideNav').classList.toggle('active');
    document.getElementById('overlay').classList.add('overlay-visible');
}

function removeNav() {
    document.getElementById('sideNav').classList.remove('active');
    document.getElementById('overlay').classList.remove('overlay-visible');
    try {
        document.getElementById('addMenu').classList.remove('add-menu-active');
    } catch (e) {
    }
}
